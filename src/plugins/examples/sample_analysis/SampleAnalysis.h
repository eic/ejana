//
// Created by yulia on 3/4/19.
//

#ifndef EJANA_SampleAnalysis_H
#define EJANA_SampleAnalysis_H

#include <TH1F.h>
#include <TH3F.h>
#include <TH2F.h>

#include <JANA/JEventProcessor.h>
#include <ejana/EServicePool.h>



class JEvent;
class JApplication;

class SampleAnalysis:public JEventProcessor
{
public:
    explicit SampleAnalysis(JApplication *);
    ~SampleAnalysis() override = default;

    //----------------------------
    // Init
    //
    // This is called once before the first call to the Process method
    // below. You may, for example, want to open an output file here.
    // Only one thread will call this.
    void Init() override;

    //----------------------------
    // Process
    //
    // This is called for every event. Multiple threads may call this
    // simultaneously. If you write something to an output file here
    // then make sure to protect it with a mutex or similar mechanism.
    // Minimize what is done while locked since that directly affects
    // the multi-threaded performance.
    void Process(const std::shared_ptr<const JEvent>& event) override;

    //----------------------------
    // Finish
    //
    // This is called once after all events have been processed. You may,
    // for example, want to close an output file here.
    // Only one thread will call this.
    void Finish() override;

private:
    TDirectory* dir_main;               /// Main TDirectory for this plugin
    ej::EServicePool services;          /// <Temporary> service locator
    TH1F* hist_pt;
};

#endif //EJANA_SampleAnalysis_H
