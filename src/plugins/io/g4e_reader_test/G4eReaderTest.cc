
#include <JANA/JEventProcessor.h>
#include <JANA/Services/JGlobalRootLock.h>
#include <TH1D.h>
#include <TFile.h>
#include <Math/Vector4D.h>
#include <ejana/MainOutputRootFile.h>
#include <MinimalistModel/McGeneratedParticle.h>
#include <fmt/core.h>
#include <io/g4e_reader/GeantCeEmcalData.h>
#include <io/g4e_reader/GeantPrimaryParticle.h>


class G4eReaderTestProcessor: public JEventProcessor {
private:
    std::shared_ptr<ej::MainOutputRootFile> m_file;
    int m_verbose;                /// verbosity level. 0-none, 1-some, 2-all
    TDirectory* m_plugin_dir;     /// Virtual sub-folder inside root file used for this specific plugin

    /// Declare histograms here
    TH1D* h1d_pt;

public:
    explicit G4eReaderTestProcessor() {
    }

    void Init() override {
        auto app = GetApplication();
        
        // Get main ROOT output file 
        m_file = app->GetService<ej::MainOutputRootFile>();

        // Set parameters that can be controlled from command line
        m_verbose = 1;
        app->SetDefaultParameter("g4e_reader_test:verbose", m_verbose, "Verbosity level 0=none, 1=some, 2=all");

        if(m_verbose) {
            fmt::print("G4eReaderTestProcessor::Init()\n  g4e_reader_test:verbose={}\n", m_verbose);
        }

        // Setup histograms
        m_plugin_dir = m_file->mkdir("g4e_reader_test"); // Create a subdir inside dest_file for these results
        m_plugin_dir->cd();
        h1d_pt = new TH1D("e_pt", "electron pt", 100,0,10);
    }

    void Process(const std::shared_ptr<const JEvent>& event) override {
        // This function is called every event
        if(m_verbose == 2) fmt::print("Begin of event {} \n", event->GetEventNumber());
                
         
        // Acquire any results you need for your analysis
        auto emcal_hits = event->Get<jleic::GeantCeEmcalData>();

        double hit_sum_etot = 0;
        // Go through emcal hits
        for(size_t i=0; i< emcal_hits.size(); i++) {
            auto hit = emcal_hits[i];

            hit_sum_etot+=hit->etot_dep;
            if(m_verbose >= 2) {
                fmt::print("emcal hit name = {:<25}  id = {:<15}  adc = {:<15.1f} \n", hit->module_name, hit->id, hit->adc);
            }
        }

        auto particles = event->Get<jleic::GeantPrimaryParticle>();
        double gen_sum_etot = 0;
        // Go through generated particles
        for(auto particle: particles) {
            gen_sum_etot += particle->tot_e;
        }

        if(m_verbose >= 2) {
            fmt::print("=== hit_sum_etot = {:<15}  gen_sum_etot = {:<15}\n", hit_sum_etot, gen_sum_etot);
        }
    }

    void Finish() override {
        fmt::print("G4eReaderTestProcessor::Finish(). Cleanup\n");
    }
};

extern "C" {
    void InitPlugin(JApplication *app) {
        InitJANAPlugin(app);
        app->Add(new G4eReaderTestProcessor);
    }
}
