
# Make sure pyjano is installed in your system
# do:
#     pip install --upgrade pyjano          # for conda, venv or root install
#     pip install --user --upgrade pyjano   # for user local install
#
# Please, wget 
from pyjano.jana import Jana, PluginFromSource

my_plugin = PluginFromSource('./', name='g4e_reader_test')

jana = Jana(nevents=1000, output='min_ex_output.root')
jana.plugin('lund_reader') \
    .plugin(my_plugin, verbose=1) \
    .source('/home/romanov/Downloads/pipi.lund') \
    .run()
