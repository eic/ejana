//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Jefferson Science Associates LLC Copyright Notice:
//
// Copyright 251 2014 Jefferson Science Associates LLC All Rights Reserved. Redistribution
// and use in source and binary forms, with or without modification, are permitted as a
// licensed user provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice, this
//    list of conditions and the following disclaimer in the documentation and/or other
//    materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products derived
//    from this software without specific prior written permission.
// This material resulted from work developed under a United States Government Contract.
// The Government retains a paid-up, nonexclusive, irrevocable worldwide license in such
// copyrighted data to reproduce, distribute copies to the public, prepare derivative works,
// perform publicly and display publicly and to permit others to do so.
// THIS SOFTWARE IS PROVIDED BY JEFFERSON SCIENCE ASSOCIATES LLC "AS IS" AND ANY EXPRESS
// OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
// JEFFERSON SCIENCE ASSOCIATES, LLC OR THE U.S. GOVERNMENT BE LIABLE TO LICENSEE OR ANY
// THIRD PARTES FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
// OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//
// Author: Nathan Brei
//

#include "EicGenfitWrapper.h"

// From Genfit
#include <ConstField.h>
#include <DAF.h>
#include <FieldManager.h>
#include <HelixTrackModel.h>
#include <MaterialEffects.h>
#include <SpacepointMeasurement.h>
#include <TGeoMaterialInterface.h>
#include <Track.h>
#include <EventDisplay.h>
#include <TApplication.h>


EicGenfitOutput FitTracks(const EicGenfitInput& input) {

    EicGenfitOutput output {};

    genfit::FieldManager::getInstance()->init(new genfit::ConstField(input.Bx, input.By, input.Bz)); // 15 kGauss / 1.5T
    genfit::MaterialEffects::getInstance()->init(new genfit::TGeoMaterialInterface());
    genfit::MaterialEffects::getInstance()->setNoEffects();

    auto fitter = new genfit::DAF(true);
    fitter->setDebugLvl(input.debug_level);

    std::vector<RecoTrack*> summaries;  // Ownership transferred to JEvent
    std::vector<genfit::Track*> tracks; // Ownership transferred to genfit EventDisplay

    for (auto cluster : input.clusters) {

        output.metadata.total_track_count += 1;

        auto hits = cluster->hits;
        if (hits.size() < 6) {
            output.metadata.insufficient_data_count += 1;
            continue; // Genfit can't fit tracks given fewer than 6 hits
        }

        // Find geant track corresponding to this cluster
        const minimodel::McTrack* truth_track = nullptr;
        for (auto* truth_track_candidate : input.truths) {
            if (truth_track_candidate->id == cluster->track_id) {
                truth_track = truth_track_candidate;
            }
        }

        // Initial state estimate
        TVector3 pos;
        TVector3 mom;

        if (truth_track != nullptr) {
            pos = TVector3 (truth_track->vtx_x / 10, truth_track->vtx_y / 10, truth_track->vtx_z / 10); // TODO: Need to convert?!?
            mom = TVector3 (truth_track->px, truth_track->py, truth_track->pz);
        }
        else {
            std::cout << "Did not find 'truth' initial conditions. " << std::endl;
            continue; // (!) can't continue here as we need true PDG

            // Initial position and momentum come straight from the first hit
            pos = TVector3 (hits[0]->Xpos/10, hits[0]->Ypos/10, hits[0]->Zpos/10); // Convert from mm to cm
            mom = TVector3 (hits[0]->Xpos, hits[0]->Ypos, hits[0]->Zpos);
        }

        TMatrixDSym covM(6);
        covM *= 0;
        for (int i = 0; i < 3; ++i) {
            covM(i,i) = input.y_res * input.y_res;
        }
        for (int i = 3; i < 6; ++i) {
            // Due to unknown initial momentum we give it bigger error
            covM(i,i) = pow(input.y_res / 2. / sqrt(3), 2);
        }

        // Set up track object with one hypothesis
        // assert(input.pdgs.size() > 0);
        // auto pdg_iter = input.pdgs.begin();
        // genfit::AbsTrackRep* rep = new genfit::RKTrackRep(truth_track->pdg);
        // pdg_iter++;
        // But we have a true hypothesis...
        // so we use this for now
        genfit::AbsTrackRep* rep = new genfit::RKTrackRep(truth_track->pdg);


        genfit::MeasuredStateOnPlane stateRef(rep);
        stateRef.setPosMomCov(pos, mom, covM);
        TVectorD seedState(6);
        TMatrixDSym seedCov(6);
        rep->get6DStateCov(stateRef, seedState, seedCov);
        auto track = new genfit::Track(rep, seedState, seedCov);

        // This is the way to add track hypothesis. But we know the true info, so we don't use it here
        //        while (pdg_iter != input.pdgs.end()) {
        //            auto pdg2 = *pdg_iter;
        //            pdg_iter++;
        //            auto rep2 = new genfit::RKTrackRep(pdg2);
        //            genfit::MeasuredStateOnPlane msop(rep2);
        //            rep2->setPosMomCov(msop, pos, mom, covM);
        //            TVectorD seedState2(6);
        //            TMatrixDSym seedCov2(6);
        //            rep2->get6DStateCov(stateRef, seedState2, seedCov2);
        //            track->addTrackRep(rep2);
        //        }

        // Add hits
        int hit_id = 0;
        for (auto hit : hits) {

            // Convert hit coords to genfit format
            TVectorD hitCoords_S(3);
            hitCoords_S(0) = hit->Xpos / 10;  // Convert from mm to cm
            hitCoords_S(1) = hit->Ypos / 10;
            hitCoords_S(2) = hit->Zpos / 10;

            // Define covariance matrix for this hit given resolution information from somewhere
            TMatrixDSym hitCov_S(3);
            hitCov_S *= 0.;
            hitCov_S(0,0) = input.y_res * input.y_res;
            hitCov_S(1,1) = input.y_res * input.y_res;
            hitCov_S(2,2) = input.t_res * input.t_res;

            auto spm = new genfit::SpacepointMeasurement(hitCoords_S, hitCov_S, input.det_id, ++hit_id, nullptr);
            track->insertMeasurement(spm);
        }
        try {
            fitter->processTrack(track);
            tracks.push_back(track);
            if (truth_track != nullptr) {
                std::cout << "Truth pdg = " << truth_track->pdg << std::endl;
            }

            auto cardinal_rep = track->getCardinalRep();
            for (auto trep : track->getTrackReps()) {
                auto fs = track->getFitStatus(trep);
                if (trep == cardinal_rep) {
                    std::cout << "Cardinal Rep: " << trep->getPDG() << " -> " << fs->isFitConverged() << " with chi2/ndf = " << fs->getChi2()/fs->getNdf() << std::endl;
                }
                else {
                    std::cout << "Rep: " << trep->getPDG() << " -> " << fs->isFitConverged() << " with chi2 = " << fs->getChi2() << std::endl;
                }
            }
            auto fs = track->getFitStatus(); // Uses the cardinal rep, which we assume = `rep`

            if (fs->isFitted() && fs->isFitConverged()) {

                auto summary = new RecoTrack;
                summary->track_id = cluster->track_id;
                summary->chisq  = fs->getChi2();
                summary->ndf    = fs->getNdf();
                summary->charge = fs->getCharge();

                const genfit::MeasuredStateOnPlane& state = track->getFittedState();
                // Get fitted state (for cardinal rep) at trackpoint 0 (corresponds to hit 0)

                summary->x = state.getPos(); // TODO: Convert back to mm
                summary->p = state.getMom();
                summary->theta = state.getMom().Theta();
                summary->phi = state.getMom().Phi();
                summaries.push_back(summary);
                output.metadata.succeeded_count += 1;
            }
            else {
                output.metadata.misfit_count += 1;
            }
        }
        catch(genfit::Exception& e) {
            output.metadata.exception_count += 1;
        }
    }
    if (input.interactive) {
        genfit::EventDisplay::getInstance()->addEvent(tracks);   // Transfer ownership of genfit track data to EventDisplay
    }
    output.tracks = std::move(summaries);

    delete fitter;
    return output;
}

