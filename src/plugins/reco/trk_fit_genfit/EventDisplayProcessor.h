//
// Created by romanov on 1/17/20.
//

#ifndef EJANA_EVENTDISPLAYPROCESSOR_H
#define EJANA_EVENTDISPLAYPROCESSOR_H


#include <JANA/JEventProcessor.h>

#include <TGeoManager.h>
#include <TEveManager.h>
#include <TEveGeoNode.h>
#include <TApplication.h>
#include <EventDisplay.h>
#include <reco/trk_fit/GeometryService.h>

class EventDisplayProcessor:public JEventProcessor {
public:

    EventDisplayProcessor(JApplication* app) : JEventProcessor(app) {};

    void Init() override;
    void Process(const std::shared_ptr<const JEvent>& event) override;
    void Finish() override;

    genfit::EventDisplay* display;
    std::string geometry = "geometryJLEIC.root";
    bool interactive = false;

};

void EventDisplayProcessor::Init() {

    GetApplication()->SetDefaultParameter("trk_fit_genfit:geometry", geometry, "Path to geometry file");
    GetApplication()->SetDefaultParameter("trk_fit_genfit:interactive", interactive, "Show genfit/EVE event display afterwards");
    //GetApplication()->GetService<GeometryService>();

    if (interactive) {

        auto man = TGeoManager::Import(geometry.c_str());
        if (man == nullptr) {
            throw JException("Unable to find geometry file '%s'", geometry.c_str());
        }
        man->DefaultColors();
        man->GetTopVolume()->Print();

        if(!gEve) {
            std::cout << "Creating EVE singleton..." << std::flush;
            TEveManager::Create();
            std::cout << " done!" << std::endl;
        }

        auto top_node = gGeoManager->GetTopNode();
        assert(top_node != nullptr);

        //Set transparency & color of geometry
        TObjArray* volumes = gGeoManager->GetListOfVolumes();
        for(int i = 0; i < volumes->GetEntriesFast(); i++) {
            auto volume = dynamic_cast<TGeoVolume*>(volumes->At(i));
            assert(volume != nullptr);
            volume->SetLineColor(12);
            volume->SetTransparency(50);
        }

        auto eve_top_node = new TEveGeoTopNode(gGeoManager, top_node);
        eve_top_node->IncDenyDestroy();
        gEve->AddElement(eve_top_node);


        display = genfit::EventDisplay::getInstance();
        // display->setOptions("G");
        // TODO: Make this configurable
    }
}

void EventDisplayProcessor::Process(const std::shared_ptr<const JEvent> &event) {
    //auto tracks = event->Get<VertexTrackerTrack>();
}

void EventDisplayProcessor::Finish() {
    if (interactive) {
        display->open();
    }
}

#endif //EJANA_EVENTDISPLAYPROCESSOR_H
