
#ifndef _RecoTrackFactory_Genfit_h_
#define _RecoTrackFactory_Genfit_h_

#include <JANA/JFactoryT.h>
#include "EicGenfitWrapper.h"
#include <reco/trk_fit/MagneticFieldService.h>
#include "RecoModel/RecoTrack.h"


class RecoTrackFactory_Genfit : public JFactoryT<RecoTrack> {

    //EicGenfitWrapper genfit_wrapper; // TODO: Bring this back as a JService

    bool interactive = false;
    bool use_truth_for_estimate = true;
    EicGenfitOutput genfit_results;
    std::shared_ptr<MagneticFieldService> magnetic_field;

public:
    RecoTrackFactory_Genfit() : JFactoryT<RecoTrack>(NAME_OF_THIS, "genfit") {};
    void Init() override;
    void ChangeRun(const std::shared_ptr<const JEvent> &event) override;
    void Process(const std::shared_ptr<const JEvent> &event) override;

};

#endif // _RecoTrackFactory_Genfit_h_
