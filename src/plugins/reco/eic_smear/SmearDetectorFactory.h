#ifndef EJANA_SMEARDETECTORFACTORY_H
#define EJANA_SMEARDETECTORFACTORY_H

#include <JANA/JFactoryT.h>
#include <JANA/JFactoryGenerator.h>
#include <JANA/Utils/JTypeInfo.h>
#include <JANA/JEvent.h>

#include <MinimalistModel/McGeneratedParticle.h>
#include <eicsmear/smear/Detector.h>

/**
 *
 *  /// This class is temprorary here while JANA2 API is getting to perfection.
    /// Here we just list JFactories defined in this plugin
    struct SmearingFactoryGenerator:public JFactoryGenerator {
        void GenerateFactories(JFactorySet *factory_set) override {
            factory_set->Add(new SmearDetectorFactory());
        }
    };
 */

/// This calss is used to provide JANA transport for Smear::Detector
struct EicSmearDetector: public JObject {
    explicit EicSmearDetector(const Smear::Detector& det):
        detector(det)
    {
    }
    Smear::Detector detector;
};


class SmearDetectorFactory: public JFactoryT<EicSmearDetector> {
public:
    SmearDetectorFactory(const Smear::Detector& detector, const std::string& detector_name):
            JFactoryT<EicSmearDetector>(JTypeInfo::demangle<SmearDetectorFactory>(), detector_name),
            _detector(detector)
    {
        SetFactoryFlag(JFactory_Flags_t::PERSISTENT);   // We don't want to recycle the factory
    }

    void Process(const std::shared_ptr<const JEvent>& event) override
    {
        event->Insert(&_detector);
    }

private:
    EicSmearDetector _detector;
};

#endif //EJANA_SMEARDETECTORFACTORY_H
