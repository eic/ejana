#ifndef EJANA_SMEARINGSTATISTICS_H
#define EJANA_SMEARINGSTATISTICS_H

#include <cstdint>

// Some statistics about the smearing process
struct EicSmearStatistics {
    uint64_t total_particles = 0;
    uint64_t null_particles = 0;
    uint64_t zero_e_smear_p = 0;
    uint64_t smear_e_zero_p = 0;
    uint64_t smear_e_smear_p = 0;
    uint64_t zero_e_zero_p = 0;
};

#endif //EJANA_SMEARINGSTATISTICS_H
