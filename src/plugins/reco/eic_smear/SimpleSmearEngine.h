#ifndef EJANA_SIMPLE_SMEAR_ENGINE_H
#define EJANA_SIMPLE_SMEAR_ENGINE_H

#include <MinimalistModel/McGeneratedParticle.h>
#include "SmearingEngineBase.h"

#include <functional>
#include <utility>

namespace ej {
namespace smear {

    class SimpleSmearEngine : public SmearingEngineBase {

    public:
        explicit SimpleSmearEngine(std::function<void (minimodel::McGeneratedParticle *, int)> func, int verbose):
                _verbose(verbose),
                _smearing_func(std::move(func)) {
        }

        SmearingResults Smear(minimodel::McGeneratedParticle &particle) override {
            _smearing_func(&particle, _verbose);
            return SmearingResults::Smeared;
        }

    private:
        int _verbose;
        std::function<void (minimodel::McGeneratedParticle *, int)> _smearing_func;
    };
}} // namespaces


#endif //EJANA_SIMPLE_SMEAR_ENGINE_H
