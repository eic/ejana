#include <TVector3.h>
#include <TRandom.h>


#include "Math/Vector4D.h"

#include <fmt/format.h>     // For format and print functions
#include <cstdlib>
#include <cmath>
#include <MinimalistModel/McGeneratedParticle.h>

void SmearPrticleJleic_v1_0_2(minimodel::McGeneratedParticle *particle, int _verbose) {
    //2020-04-23

    ROOT::Math::PxPyPzMVector p;
    TVector3 vertex;
    double new_e = particle->tot_e;
    double new_px = particle->px;
    double new_py = particle->py;
    double new_pz = particle->pz;

    bool is_smeared_e = false;      /// Energy was smeared
    bool is_smeared_p = false;      /// Momentum was smeared
    bool is_smeared_vtx = false;    /// Vertex info was smeared

    p.SetPxPyPzE(particle->px, particle->py, particle->pz, particle->tot_e);
    vertex.SetXYZ(particle->vtx_x * 1000., particle->vtx_y * 1000., particle->vtx_z * 1000.);   // convert to um

    // SMEAR ENERGY
    // sigma (E) = a *sqrt(E)  (+)  b*E (+) c
    //

    // c1=gRandom->Gaus(p.E(), 0.02 * sqrt(p.E());
    // c2 =gRandom->Gaus(p.E(), 0.001* p.E());
    // c3=gRandom->Gaus(p.E(), 0.005);
    // Sigma = c1+c2+c3
    // E_new=p.E() + Sigma;    if (abs(particle->pdg) == 11 || particle->pdg == 22) {
    //  printf("Eta: oc_particle->p.Eta()=%f \n ", oc_particle->p.Eta());
    double sigma1 = 0., sigma2 = 0., sigma3 = 0.;

    if (p.Eta() > -3.5 && p.Eta() < -1.1) {
        sigma1 = gRandom->Gaus(0., 0.02 * sqrt(p.E()));
        sigma2 = gRandom->Gaus(0., 0.001 * p.E());
        sigma3 = gRandom->Gaus(0., 0.005);
        new_e = p.E() + sigma1 + sigma2 + sigma3;
        is_smeared_e = true;
    } else if (p.Eta() > -1.1 && p.Eta() < 3.5) {

        sigma1 = gRandom->Gaus(0., 0.1 * sqrt(p.E()));
        sigma2 = gRandom->Gaus(0., 0.01 * p.E());
        sigma3 = gRandom->Gaus(0., 0.005);
        new_e = p.E() + sigma1 + sigma2 + sigma3;
        is_smeared_e = true;
    }


    // Neutral particles in hadronic calorimeter
    if (particle->charge == 0 && particle->pdg != 22) {
        new_e = gRandom->Gaus(p.E(), 1 * sqrt(p.E()));
        is_smeared_e = true;
    }

    // Neutron coming to Zero Degree calorimeter
    if (abs(particle->pdg) == 2112 && p.Theta() < 0.01) {
        // zdc
        new_e = gRandom->Gaus(p.E(), 1. * sqrt(p.E()));
        is_smeared_e = true;
    }


    // EM particles
    if (particle->charge != 0) {

        // SMEAR Vertex
        if (_verbose >= 2)
            fmt::print("VERTEX smearing1: {} ", vertex.Mag());

        double x, y, z;
        double vtx_smear = fabs(gRandom->Gaus(0, 25.));
        gRandom->Sphere(x, y, z, abs(vtx_smear));


        if (vertex.Mag() < 0.01) {
            vertex.SetXYZ(x, y, z);
        } else {
            TVector3 vertex_shift(x, y, z);
            vertex = vertex + vertex_shift;
        }
        is_smeared_vtx = true;

        if (_verbose >= 2) {
            printf("VERTEX smearing2: %f, \n", vertex.Mag());
        }

// ---- SMEARING Momentum
        if (p.Eta() > -3.5 && p.Eta() < 3.5) {
            // if (oc_particle->p.Eta() > -1.1 && oc_particle->p.Eta() < 1.1) {
            double c1 = gRandom->Gaus(0, 0.01 * pow(p.Pt(), 2));
            double c2 = gRandom->Gaus(0, 0.005 * p.Pt());

            double_t mynewPt = p.Pt() + c1 + c2;
            double_t mynewTheta = gRandom->Gaus(p.Theta(), 0.001);
            double_t mynewPhi = gRandom->Gaus(p.Phi(), 0.001);

            new_px = mynewPt * sin(mynewPhi);
            new_py = mynewPt * cos(mynewPhi);
            new_pz = mynewPt / tan(mynewTheta);
            is_smeared_p = true;
        } else if (p.Eta() > -3.5 && p.Eta() < -1.1) {
            // TODO  separated smearing for e-endcap
        } else if (p.Eta() > 1.1 && p.Eta() < 3.5) {
            // TODO separated smearing for h-endcap
        } else {
            if (p.Theta() < 0.05) {
                double mynewPt;

                // p.Theta() < 0.01 => Particle goes to Roman Pot
                // 0.01 < p.Theta() < 0.05 => Particle goes to D1 area
                mynewPt = p.Theta() < 0.01 ?
                          gRandom->Gaus(p.Pt(), 0.02) :   // Roman Pot 20 MeV ? (we don't know)
                          gRandom->Gaus(p.Pt(), 0.01);   // D1 area 10 MeV ?

                // Putting it back:
                new_px = mynewPt * sin(p.Phi());
                new_py = mynewPt * cos(p.Phi());
                new_pz = mynewPt / tan(p.Theta());
                is_smeared_p = true;
            }
        }
// oc_particle->p.SetPxPyPzE(new_px, new_py, new_pz, gen_part->tot_e);
    }

    particle->has_smear_info = true;
    particle->smear.has_e = is_smeared_e;
    particle->smear.has_p = is_smeared_p;
    particle->smear.has_vtx = is_smeared_vtx;
    particle->smear.has_any_eppid = is_smeared_e || is_smeared_p;

    p.SetPxPyPzE(new_px, new_py, new_pz, new_e);

    particle->px = new_px;
    particle->py = new_py;
    particle->pz = new_pz;
    particle->tot_e = new_e;
    particle->vtx_x = vertex.x() / 1000.;
    particle->vtx_y = vertex.y() / 1000.;
    particle->vtx_z = vertex.z() / 1000.;
}
