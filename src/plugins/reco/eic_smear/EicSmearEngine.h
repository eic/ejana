#ifndef EJANA_EICSMEARENGINE_H
#define EJANA_EICSMEARENGINE_H

#include <MinimalistModel/McGeneratedParticle.h>
#include "SmearingEngineBase.h"
#include "eicsmear/smear/Detector.h"
#include "eicsmear/smear/ParticleMCS.h"
#include "eicsmear/erhic/ParticleMC.h"

namespace ej {
namespace smear {

    class EicSmearEngine : public SmearingEngineBase {
    public:

        EicSmearEngine (Smear::Detector detector):
                _detector(std::move(detector))
        {
        }

        virtual SmearingResults Smear(minimodel::McGeneratedParticle &particle) {

            // EIC smear
            erhic::ParticleMC not_smeared_prt;
            not_smeared_prt.SetId(particle.pdg);           // PDG particle code
            not_smeared_prt.SetIndex(particle.id);         // Particle index in event
            not_smeared_prt.SetStatus((int)particle.gen_code);  // Particle status code: like in PYTHIA, Beagle, etc
            not_smeared_prt.Set4Vector(TLorentzVector(particle.px, particle.py, particle.pz, particle.tot_e));
            not_smeared_prt.SetP(particle.p);

            // input p and e
            double in_p = particle.p;
            double in_e = particle.tot_e;

            Smear::ParticleMCS * smeared_prt = _detector.Smear(not_smeared_prt);

            if(!smeared_prt) {
                return SmearingResults::NotGoneThrough;
            }

            // fmt::print("P {:<10} {:<10}  E {:<10}  {:<10}\n", source_particle->p, smeared_prt->GetP(), source_particle->tot_e, smeared_prt->GetE());

            // Copy back the particle
            particle.px = smeared_prt->GetPx();
            particle.py = smeared_prt->GetPy();
            particle.pz = smeared_prt->GetPz();
            particle.p = smeared_prt->GetP();
            particle.tot_e = smeared_prt->GetE();

            if(FP_ZERO != std::fpclassify(particle.p))   // Check p is not zero for the next division
            {
                /// Direction unit vector component at vertex
                particle.dir_x  = particle.px / particle.p;
                particle.dir_y  = particle.py / particle.p;
                particle.dir_z  = particle.pz / particle.p;
            } else {
                particle.dir_x = 0;
                particle.dir_y = 0;
                particle.dir_z = 0;
            }


            // Check odds of eic-smear smearing
            double sm_p = smeared_prt->GetP();
            double sm_e = smeared_prt->GetE();

            // Eic smear return non zero p
            bool zero_p = TMath::Abs(in_p)>0.00001 && TMath::Abs(sm_p)<0.00001;
            bool zero_e = TMath::Abs(in_e)>0.00001 && TMath::Abs(sm_e)<0.00001;

            if(zero_p && zero_e) {
                particle.smear.has_any_eppid = false;
            } else if (zero_p) {
                particle.smear.has_any_eppid = true;
                particle.smear.has_e = true;
            } else if (zero_e) {
                particle.smear.has_any_eppid = true;
                particle.smear.has_p = true;
            } else {
                particle.smear.has_any_eppid = true;
                particle.smear.has_p = true;
                particle.smear.has_e = true;
            }
            return SmearingResults::Smeared;
        }

    private:
        Smear::Detector _detector;

    };

}} // namespaces


#endif //EJANA_EICSMEARENGINE_H
