//
// Created by romanov on 7/11/2019.
//

#ifndef EJANA_SMEARINGFACTORY_H
#define EJANA_SMEARINGFACTORY_H
#include <mutex>

#include <JANA/JFactoryT.h>
#include <JANA/Services/JParameterManager.h>
#include <JANA/Utils/JTypeInfo.h>

#include <MinimalistModel/McGeneratedParticle.h>
#include <eicsmear/smear/Detector.h>

#include <functional>
#include "SmearingEngineBase.h"
#include "SmearingStatistics.h"

enum class SmearingEngines {
    EIC_SMEAR,
    SIMPLE_SMEAR
};

class SmearingFactory: public JFactoryT<minimodel::McGeneratedParticle> {
public:

    explicit SmearingFactory(const std::string& aTag = "smear") :
        JFactoryT<minimodel::McGeneratedParticle>(JTypeInfo::demangle<SmearingFactory>(), aTag)
    {
    }

    virtual ~SmearingFactory() {
        PrintSmearStats();
    }

    void Init() override;

    /// Process is called every event when smearing is required
    void Process(const std::shared_ptr<const JEvent>& event) override;

    /// Returns total smearing statistics
    static EicSmearStatistics& GetStatistics() {
        static EicSmearStatistics _stat;
        return _stat;
    }

    /// Fancy print of smearing statistics
    static void PrintSmearStats();


private:
    std::once_flag& GetWarnNoNameWarningOnceFlag() {
        static std::once_flag _warn_no_name_provided_once_flag;  // This is used to tell once, that detector name is not given
                                                                 // and smearing is not performed
        return _warn_no_name_provided_once_flag;
    }

    /// Collection of detectors
    std::map<std::string, std::shared_ptr<ej::smear::SmearingEngineBase>> _detectors;

    /// The smearing engine that we really use
    std::weak_ptr<ej::smear::SmearingEngineBase> _selected_engine;

    std::string _detector_name;                /// detector name to smear
    int _verbose = 0;                          /// verbose output level 0 - none, 1 - info, 2 - all
    bool _throw_not_smeared = true;            /// throw particles which are not smeared
    bool _strict_particle_selection = false;   /// False - leave particles if at least anything is smeared. True - leave only particles with both e and p smeared
};

#endif //EJANA_SMEARINGFACTORY_H
