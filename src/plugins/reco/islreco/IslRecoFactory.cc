#include <JANA/JFactoryT.h>
#include <JANA/JFactoryGenerator.h>
#include <JANA/Utils/JTypeInfo.h>
#include <JANA/JEvent.h>
#include <JANA/JEventProcessor.h>
#include <JANA/Services/JGlobalRootLock.h>
#include <TH1D.h>
#include <TFile.h>
#include <Math/Vector4D.h>
#include <ejana/MainOutputRootFile.h>
#include <MinimalistModel/McGeneratedParticle.h>
#include <fmt/core.h>
#include <cstdlib>
#include <cmath>
#include <unistd.h>
#include "ilreco.h"
#include <io/g4e_reader/GeantCeEmcalData.h>
#include <io/g4e_reader/GeantPrimaryParticle.h>
#include "CeEmcalCluster.h"

struct IslRecoRootRecord {

};


class IslRecoFactory: public JFactoryT<CeEmcalCluster> {

    int m_verbose;                     /// verbosity level. 0-none, 1-some, 2-all
    std::string m_crystal_prof_file;   /// File of the shower profile
    std::string m_glass_prof_file;     /// Glass profile file
    double m_cell_adc_cut;             /// Removes cell from calculation if its deposit is smaller than the value
    profile_t m_crystal_prof;          /// Crystal profile
    profile_t m_glass_prof;            /// Glass profile

public:
    IslRecoFactory()
    {

    }

    void Init() override {
        auto app = GetApplication();

        // Set parameters that can be controlled from command line
        m_verbose = 1;
        app->SetDefaultParameter("islreco:verbose", m_verbose, "Verbosity level 0=none, 1=some, 2=all");

        m_crystal_prof_file = std::string();
        app->SetDefaultParameter("islreco:inner_profile", m_crystal_prof_file, "Shower profile data file for inner region (crystal)");

        m_glass_prof_file = std::string();
        app->SetDefaultParameter("islreco:outer_profile", m_glass_prof_file, "Shower profile data file for outer region (glass)");

        // Cell deposit cut
        m_cell_adc_cut = 10;
        app->SetDefaultParameter("islreco:adc_cut", m_cell_adc_cut, "Removes cell from calculation if its deposit is smaller than the value");

        if(m_verbose) {
            fmt::print("CalIslandReconProcessor::Init()\n |\n", m_verbose);
            fmt::print(" +-> islreco:verbose={}\n", m_verbose);
            fmt::print(" +-> islreco:inner_profile={}\n", m_crystal_prof_file);
            fmt::print(" +-> islreco:outer_profile={}\n", m_glass_prof_file);
            fmt::print(" +-> islreco:adc_cut={}\n", m_cell_adc_cut);
        }

        if(m_crystal_prof_file.empty()) {
            char* profile_fname = getenv ("ISLRECO_CENTER_PROFILE");

            if (profile_fname == nullptr) {
                throw JException("ISLRECO: ERROR islreco:inner_profile is empty and ISLRECO_CENTER_PROFILE environment isn't set");
            }
            m_crystal_prof_file = profile_fname;
        }

        if(m_glass_prof_file.empty()) {
            char* profile_fname = getenv ("ISLRECO_OUTER_PROFILE");

            if (profile_fname == nullptr) {
                throw JException("ISLRECO: ERROR islreco:outer_profile is empty and ISLRECO_OUTER_PROFILE environment isn't set");
            }
            m_glass_prof_file = profile_fname;
        }

        LoadProfile(&m_crystal_prof, m_crystal_prof_file);
        LoadProfile(&m_glass_prof, m_glass_prof_file);
    }

    /// Loads ilreco profile file from a file using file_name
    void LoadProfile(profile_t* profile, const std::string& file_name) {


        if(access(m_crystal_prof_file.c_str(), F_OK ) == 0 ) {
            // file exists
            if(m_verbose) {
                fmt::print("islreco: File exists. Opening profile: '{}'...\n", file_name);
            }
        } else {
            // file doesn't exist
            std::string message(fmt::format("(!) islreco: ERROR profile file '{}' doesn't exists\n", file_name));
            throw JException(message);
        }

        // Reading islreco profile
        read_profile_data(profile, file_name.c_str());
    }

    /// Processes one section: Outer (glass) or Inner (crystal)
    void ProcessCalSection(std::vector<const jleic::GeantCeEmcalData*> hits, std::vector<CeEmcalCluster *> &reco_clusters, profile_t *profile, const std::string& section_name, uint64_t section_id)
    {
        using namespace fmt;

        int nadcgam, ncl;                   // number of particles and clusters
        int addresses[_MADR_];              // arrays of addresses
        double energies[_MADR_];            //        and energies
        int lencl[_MCL_];                   // array of clusters lengths
        adcgam_t adcgam[_MADCGAM_];         // final reconstructions storage


        int hits_count = 0;
        double hit_sum_etot = 0;
        std::unordered_map<int, const jleic::GeantCeEmcalData*> hits_by_address;


        // Go through emcal hits

        // >oO debug printing
        if(m_verbose >= 3) {
            fmt::print("   islreco: {:<16} emcal hits (n={}): [ module name | id | adc ]:\n", section_name, hits.size());
        }

        // go over the loop
        for(size_t i=0; i < hits.size(); i++) {
            auto hit = hits[i];

            int address = hit->row + hit->col * _OFFSET_;

            // Add hit to the map, which will be used later
            hits_by_address[address] = hit;

            if(hit->adc < m_cell_adc_cut) continue;  // Cut cells with less then 10MeV

            // (!) This is a caveat (!)
            // ia and id should start with index 1
            // It is a fortran ported code
            hits_count++;
            addresses[hits_count] = address;
            energies[hits_count] = hit->etot_dep / 1000.0;

            // Fill some random
            hit_sum_etot+=hit->etot_dep;
            if(m_verbose >= 3) {
                fmt::print("      {:<25}  {:<15}  {:<15.1f} \n", hit->module_name, hit->id, hit->adc);
            }
        }

        // Islreco methods
        nadcgam = 0;                      // reset number of rec-d particles
        ncl = cluster_search(profile, hits_count, addresses, energies, lencl);         //  1st stage cluster processing
        for (int icl = 1, ipncl = 1; icl <= ncl && nadcgam < _MADCGAM_ - 2; ipncl += lencl[icl++]) {
            process_cluster(profile, lencl[icl], &addresses[ipncl - 1], &energies[ipncl - 1], &nadcgam, adcgam);    //  2nd stage cluster processing
        }

        // FILL Factory hits
        for(int i = 1; i <= nadcgam; ++i) {
            auto cal_cluster = new CeEmcalCluster();
            cal_cluster->id = i - 1;
            cal_cluster->section_id = section_id;
            cal_cluster->energy = adcgam[i].e;
            cal_cluster->rel_x = adcgam[i].x[1];
            cal_cluster->rel_y = adcgam[i].y[1];
            cal_cluster->abs_x = (cal_cluster->rel_x - 40) * (20 + 0.1);
            cal_cluster->abs_y = (cal_cluster->rel_y - 40) * (20 + 0.1);
            cal_cluster->chi2 = adcgam[i].chi2;
            cal_cluster->cluster_size = adcgam[i].size;
            cal_cluster->type = adcgam[i].type;

            // Adding information on cluster cells
            for(size_t cell_iter=1; cell_iter <= adcgam[i].size; cell_iter++) {
                CeEmcalClusterCell cell_info;
                cell_info.cluster_id = cal_cluster->id;
                cell_info.address = adcgam[i].element[cell_iter];
                cell_info.cell_fraction = adcgam[i].elfract[cell_iter];

                cell_info.cell_global_id = hits_by_address[cell_info.address]->id;
                cell_info.cell_total_energy = hits_by_address[cell_info.address]->etot_dep;
                cell_info.cell_total_adc = hits_by_address[cell_info.address]->adc;
                cal_cluster->cells.push_back(cell_info);
            }

            //adcgam[i].stat, adcgam[i].id);
            reco_clusters.push_back(cal_cluster);
        }


        //  fill_clusters();
        if(m_verbose >= 2) {
            if(m_verbose >= 4) {
                if(ncl) {
                    print("   islreco: {:<16} dump clusters, num cl.= {}. Hits:\n", section_name, ncl);
                    for (int cluster_index = 1, ipncl = 1; cluster_index <= ncl && nadcgam < _MADCGAM_ - 2; ipncl += lencl[cluster_index++]) {
                        printf("      cluster_index=%3i lencl=%4i [cell_index | address | energy]:\n", cluster_index, lencl[cluster_index]);
                        for(int cell_index = 1; cell_index < lencl[cluster_index]; cell_index++) {
                            auto address = addresses[ipncl - 1 + cell_index];
                            auto energy = energies[ipncl - 1 + cell_index];
                            print("         {:<10} {:<10} {:<10}\n", cell_index-1, address, energy);
                        }
                    }
                }
            }

            if(nadcgam) {
                print("   islreco: {:<16} reco clusters:\n", section_name);
                for(int i = 1; i <= nadcgam; ++i) {
                    print("      {:<3} e={:<8.4f} x={:<8.3f} y={:<8.3f} x0={:<8.3f} y0={:<8.3f} chi2={:<8.4f} cl_size={:<3} type={:<3} stat={:<2}  id={:<3}\n",
                          i-1, adcgam[i].e, adcgam[i].x[1], adcgam[i].y[1], adcgam[i].x[0], adcgam[i].y[0],  adcgam[i].chi2, adcgam[i].size, adcgam[i].type, adcgam[i].stat, adcgam[i].id);
                }
            }
            else
            {
                print("   islreco: {:<16} no clusters found\n", section_name);
            }
        }
    }

    void Process(const std::shared_ptr<const JEvent>& event) override
    {
        // This function is called every event
        if(m_verbose >= 2) fmt::print("==>islreco: event # {} \n", event->GetEventNumber());

        // Acquire any results you need for your analysis
        auto emcal_hits = event->Get<jleic::GeantCeEmcalData>();

        std::vector<const jleic::GeantCeEmcalData*> inner_hits;
        std::vector<const jleic::GeantCeEmcalData*> outer_hits;

        for(size_t i=0; i < emcal_hits.size(); i++) {
            // Glass (outer) section has id=1, Crystals (inner) has id = 0
            if(emcal_hits[i]->section_id==0) {
                inner_hits.push_back(emcal_hits[i]);
            } else {
                outer_hits.push_back(emcal_hits[i]);
            }
        }

        std::vector<CeEmcalCluster *> reco_clusters;
        ProcessCalSection(inner_hits, reco_clusters, &m_crystal_prof, "[inner(crystal)]", 0);
        ProcessCalSection(outer_hits, reco_clusters, &m_glass_prof, "[outer(glass)]", 1);

        event->Insert(reco_clusters);
    }

private:

};

extern "C" {
void InitPlugin(JApplication *app) {
    InitJANAPlugin(app);
    app->Add(new JFactoryGeneratorT<IslRecoFactory>());
}
}
