//
// Created by Nathan Brei on 3/5/20.
//

#ifndef EJANA_MAGNETICFIELDSERVICE_H
#define EJANA_MAGNETICFIELDSERVICE_H


#include <JANA/Services/JServiceLocator.h>

class MagneticFieldService : public JService {

public:

    ~MagneticFieldService() override = default;
    void acquire_services(JServiceLocator* sl) override;
    double Bx = 0.;
    double By = 0.;
    double Bz = 20;  // Measured in kGauss. Remember that 1 T = 10 kGauss

};


#endif //EJANA_MAGNETICFIELDSERVICE_H
