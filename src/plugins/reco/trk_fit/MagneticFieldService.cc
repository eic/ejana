//
// Created by Nathan Brei on 3/5/20.
//

#include <JANA/Services/JParameterManager.h>
#include "MagneticFieldService.h"

void MagneticFieldService::acquire_services(JServiceLocator *sl) {

    auto params = sl->get<JParameterManager>();
    params->SetDefaultParameter("Bx", Bx, "Strength of constant magnetic field in kGauss in x direction");
    params->SetDefaultParameter("By", By, "Strength of constant magnetic field in kGauss in y direction");
    params->SetDefaultParameter("Bz", Bz, "Strength of constant magnetic field in kGauss in z direction");
}
