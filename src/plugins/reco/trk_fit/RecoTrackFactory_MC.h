
#ifndef _RecoTrackFactory_MC_h_
#define _RecoTrackFactory_MC_h_

#include <JANA/JFactoryT.h>

#include "RecoModel/RecoTrack.h"

class RecoTrackFactory_MC : public JFactoryT<RecoTrack> {

    // Insert any member variables here

public:
    RecoTrackFactory_MC() : JFactoryT<RecoTrack>(NAME_OF_THIS, "truths") {};
    ~RecoTrackFactory_MC() = default;

    void Init() override;
    void ChangeRun(const std::shared_ptr<const JEvent> &event) override;
    void Process(const std::shared_ptr<const JEvent> &event) override;

};

#endif // _RecoTrackFactory_MC_h_
