
https://g4e.readthedocs.io/en/latest/01_quick_start.html

Batch mode (no event display),

```bash
g4e cone_pgun.mac -o my_file
```

In order to switch event display on or off, one has to provide a flag and make modification to a mac file

```
/control/execute jleic.mac          # batch mode
#/control/execute jleicvis.mac      # event display mode
``` 

Event display command

```bash
g4e --gui cone_pgun.mac -o my_file
```