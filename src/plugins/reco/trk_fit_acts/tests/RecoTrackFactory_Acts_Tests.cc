#include "external/catch.hpp"

#include <JANA/JEvent.h>
#include <RecoModel/RecoHit.h>
#include <RecoModel/RecoTrackCand.h>

#include "reco/trk_fit_acts/ActsPrimaryVertexFitter.h"

TEST_CASE("RecoVertexFactory_Acts_Test") {

    // This is our "system-under-test"
    auto sut = new ActsPrimaryVertexFitter;

    // Create a new event object and attach the factory to our event
    auto event = std::make_shared<JEvent>();
    auto factory_set = new JFactorySet;
    factory_set->Add(sut);
    event->SetFactorySet(factory_set);

    // Feed some input data into the event object
    auto cluster = new RecoTrackCand;
    auto hit1 = new RecoHit;
    hit1->track_id = 0;
    hit1->Xpos = 0;
    hit1->Ypos = 0;
    hit1->Zpos = 0;
    cluster->hits.push_back(hit1);
    event->Insert(cluster);

    // JEvent will call Init, Process, and ChangeRun as needed for us
    auto results = event->Get<RecoTrack>();

    // Assert something based on results calculated or changes to factory members
    REQUIRE(true == true);
}



TEST_CASE("RecoTrackFactory_Acts_UnitTest") {

    // This is our "system-under-test"
    auto sut = new ActsPrimaryVertexFitter;

    // Create a new event object
    auto event = std::make_shared<JEvent>();
    event->SetFactorySet(new JFactorySet);
    auto app = new JApplication;

    // Feed some input data into the event object
    auto cluster = new RecoTrackCand;
    auto hit1 = new RecoHit;
    hit1->track_id = 0;
    hit1->Xpos = 0;
    hit1->Ypos = 0;
    hit1->Zpos = 0;
    cluster->hits.push_back(hit1);
    event->Insert(cluster);

    auto pair = sut->GetOrCreate(event, app, 0);

    // Assert something based on results calculated or changes to factory members
    REQUIRE(true == true);
}


