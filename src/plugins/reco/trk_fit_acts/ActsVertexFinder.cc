
#include "ActsVertexFinder.h"

#include <JANA/JEvent.h>
#include <random>

#include "Acts/MagneticField/ConstantBField.hpp"
#include "Acts/Propagator/EigenStepper.hpp"
#include "Acts/Propagator/Propagator.hpp"
#include "Acts/Vertexing/HelicalTrackLinearizer.hpp"
#include "Acts/Vertexing/FullBilloirVertexFitter.hpp"
#include "Acts/Vertexing/ImpactPointEstimator.hpp"
#include "Acts/Vertexing/IterativeVertexFinder.hpp"
#include "Acts/Utilities/Units.hpp"


void ActsVertexFinder::Init() {
    auto app = GetApplication();
    
    /// Acquire any parameters
    app->SetDefaultParameter("trk_fit_acts:reco_track_factory:input", m_reco_track_input, "Factory tag for RecoCluster");

    /// Acquire any services
    // m_service = app->GetService<ServiceT>();
    
    /// Set any factory flags
    // SetFactoryFlag(JFactory_Flags_t::NOT_OBJECT_OWNER);
}

void ActsVertexFinder::ChangeRun(const std::shared_ptr<const JEvent> &event) {
    /// This is automatically run before Process, when a new run number is seen
    /// Usually we update our calibration constants by asking a JService
    /// to give us the latest data for this run number
    
    // auto run_nr = event->GetRunNumber();
    // m_calibration = m_service->GetCalibrationsForRun(run_nr);
}

void ActsVertexFinder::Process(const std::shared_ptr<const JEvent> &event) {

    // Most of this comes from acts-framework/Algorithms/Vertexing/src/VertexFindingAlgorithm.cpp

    using namespace Acts::UnitLiterals;
    using MagneticField     = Acts::ConstantBField;
    using Stepper           = Acts::EigenStepper<MagneticField>;
    using Propagator        = Acts::Propagator<Stepper>;
    using PropagatorOptions = Acts::PropagatorOptions<>;
    using TrackParameters   = Acts::BoundTrackParameters;
    using Linearizer        = Acts::HelicalTrackLinearizer<Propagator>;
    using VertexFitter = Acts::FullBilloirVertexFitter<TrackParameters, Linearizer>;
    using VertexSeederImpactPointEstimator = Acts::ImpactPointEstimator<TrackParameters, Propagator>;
    using VertexSeeder = Acts::ZScanVertexFinder<VertexFitter>;
    using VertexFinder = Acts::IterativeVertexFinder<VertexFitter, VertexSeeder>;
    using VertexFinderOptions = Acts::VertexingOptions<TrackParameters>;

    Acts::GeometryContext geo_context; // TODO: Figure out what ACTS does to initialize these
    Acts::MagneticFieldContext field_context;
	auto logger = Acts::getDefaultLogger("ActsVertexFinder", Acts::Logging::Level::INFO);

	// Set up the magnetic field
    MagneticField bField(Acts::Vector3D(0,0,2_T));
    // TODO: Parameterize me
    // TODO: Why don't we add the bField to the field_context?

    // Set up propagator with void navigator
    auto propagator = std::make_shared<Propagator>(Stepper(bField));
    PropagatorOptions propagatorOpts(geo_context, field_context, Acts::LoggerWrapper(*logger));
    propagatorOpts.maxSteps = 100;

    // Setup the vertex fitter
    VertexFitter::Config vertexFitterCfg;
    VertexFitter vertexFitter(std::move(vertexFitterCfg));

    // Setup the track linearizer
    Linearizer::Config linearizerCfg(bField, propagator);
    Linearizer linearizer(std::move(linearizerCfg));

    // Setup the seed finder
    VertexSeederImpactPointEstimator::Config seederIpEstCfg(bField, propagator);
    VertexSeederImpactPointEstimator seederIpEst(std::move(seederIpEstCfg));
    VertexSeeder::Config seederCfg(std::move(seederIpEst));
    VertexSeeder seeder(std::move(seederCfg));


    // Set up the actual vertex finder
    VertexFinder::Config finderCfg(std::move(vertexFitter),
                                   std::move(linearizer),
                                   std::move(seeder),
                                   std::move(seederIpEstCfg));
    VertexFinder::State state {nullptr};

    finderCfg.maxVertices = 200; // TODO: Parameterize me!
    finderCfg.reassignTracksAfterFirstFit = true;  // TODO: What does this do?

    VertexFinder finder(finderCfg, Acts::getDefaultLogger("IterativeVertexFinder", Acts::Logging::VERBOSE));
    VertexFinderOptions finderOpts(geo_context, field_context);

    // This is a wild guess, see FittingAlgorithm.cpp
    // TODO: Rethink this
    auto surface = Acts::Surface::makeShared<Acts::PerigeeSurface>(Acts::Vector3D{0., 0., 0.});

    // This factory attempts to obtain existing tracks, currently from Genfit (default) or "truths"
    auto tracks = event->Get<RecoTrack>(m_reco_track_input);

    std::vector<Acts::BoundTrackParameters> acts_tracks;


    for (auto track : tracks) {

        // Convert RecoTrack to Acts::BoundParameters

        PropagatorOptions pOptions2(geo_context, field_context, Acts::LoggerWrapper{*logger});
        pOptions2.direction = Acts::backward;

	    double p = track->p.Mag();
        Acts::ActsVectorD<4> position(track->x.x(), track->x.y(), track->x.z(), 0); // Space units are mm
	    Acts::ActsVectorD<3> momentum(track->p.Px(), track->p.Py(), track->p.Pz()); // Units are GeV
	    Acts::ActsVectorD<3> dirs(track->p.Px()/p, track->p.Py()/p, track->p.Pz()/p);
	    double q_over_p = track->charge / p;


	    /// Do track smearing
        bool doSmearing = true;

        /// Define parameter for pt-dependent IP resolution
        /// of the form sigma_d/z(p_t[GeV]) = A*exp(-B*p_t[GeV]) + C
        double ipResA = 30_um;
        double ipResB = 0.3 / 1_GeV;
        double ipResC = 20_um;

        /// Angular resolution
        double angRes = 0.05;
        /// q/p relative resolution factor
        double qpRelRes = 0.001;

        const double particlePt = Acts::VectorHelpers::perp(momentum);

        const double ipRes = ipResA * std::exp(-ipResB * particlePt) + ipResC;

        // except for IP resolution, following variances are rough guesses
        // Gaussian distribution for IP resolution
        //std::normal_distribution<double> gaussDist_IP(0., ipRes);
        // Gaussian distribution for angular resolution
        //std::normal_distribution<double> gaussDist_angular(0., angRes);
        // Gaussian distribution for q/p (momentum) resolution


        double rn_d0 = ipRes;
        double rn_z0 = ipRes;
        double rn_ph = angRes;
        double rn_th = angRes;
        double rn_qp = qpRelRes;

        Acts::BoundSymMatrix covMat;
        covMat.setZero();
        covMat.diagonal() << rn_d0 * rn_d0, rn_z0 * rn_z0, rn_ph * rn_ph, rn_th * rn_th, rn_qp * rn_qp, 1.;
        //covMat.diagonal() << 1., 1., 1., 1., 1., 1.;

        // This weirdness taken from TruthVerticesToTracks
        // Acts::CurvilinearTrackParameters start_track(covMat, position, momentum, charge, time);
	    Acts::CurvilinearTrackParameters start_track(position, dirs, q_over_p, covMat);
	    LOG << "VertexFinder: From " << start_track << LOG_END;
        auto result = propagator->propagate(start_track, *surface, pOptions2);
        if (!result.ok()) {
            LOG << "VertexFinder: Propagate failed." << LOG_END;
            continue;
        }

        //acts_tracks.emplace_back(geo_context, cov, position, momentum, charge, time, surface);

        auto end_track = (*result).endParameters->parameters();
        auto cov = (*result).endParameters->covariance();

        auto end_track_bound = Acts::BoundTrackParameters(surface, end_track, track->charge, covMat);
        LOG << "VertexFinder: To " << end_track_bound << LOG_END;

        acts_tracks.push_back(end_track_bound);
    }

    std::vector<const Acts::BoundTrackParameters*> inputTrackPtrCollection;
    for (const auto& trk : acts_tracks) {
        inputTrackPtrCollection.push_back(&trk);
    }

    std::vector<RecoTrack*> reco_tracks;
    auto result = finder.find(inputTrackPtrCollection, finderOpts, state);

    if (result.ok()) {
        // Retrieve vertices found by vertex finder
        auto vertices = *result;


        for (auto vertex : vertices) {

            LOG << "Found vertex at " << vertex.position() << " containing tracks " << LOG_END;

            for (auto acts_track : vertex.tracks()) {
                LOG << "  " << acts_track.fittedParams << LOG_END;
                // TODO: I'm pretty sure there is a way to embed our custom RecoTrack object
                //       inside the templated BoundParameters.

                // Convert BoundParameters back to RecoTrack
                auto reco_track = new RecoTrack();
                auto pos = acts_track.fittedParams.position(geo_context);
                auto mom = acts_track.fittedParams.momentum();

                reco_track->x.SetXYZ(pos.x(), pos.y(), pos.z());
                reco_track->p.SetXYZ(mom.x(), mom.y(), mom.z());
                reco_track->charge = acts_track.fittedParams.charge();
                reco_track->chisq = acts_track.chi2Track;
                reco_track->ndf = acts_track.ndf;

                reco_tracks.push_back(reco_track);
                // TODO: Set phi and theta. Or just remove them from RecoTrack because they are redundant
            }
        }
    } else {
        LOG << "ACTS: Error with vertex finding: " << result.error() << LOG_END;
    }

    Set(reco_tracks);

    // Forward any (e.g.) genfit metadata over to us, so that we don't lose any stats
    // TODO: Something better. Probably add number of vertices fitted, whether vertex fitting attempted at all.
    auto metadata = event->GetMetadata<RecoTrack>(m_reco_track_input);
    SetMetadata(metadata);

}
