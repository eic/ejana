
#ifndef _RecoVertexFactory_Acts_h_
#define _RecoVertexFactory_Acts_h_

#include <JANA/JFactoryT.h>
#include <RecoModel/RecoVertex.h>
#include <Acts/Utilities/Definitions.hpp>
#include <Acts/Utilities/Units.hpp>

#include "RecoModel/RecoTrack.h"

using namespace Acts::UnitLiterals;

class ActsPrimaryVertexFitter : public JFactoryT<RecoTrack> {

    std::string reco_track_input = "truths"; // Options are "truths", "genfit"

    double y_res = 10; // cm?
    double t_res = 10; // TODO: Resolution keyed off of volume name

    std::vector<int> pdgs {211,11}; // Pi+ (+1) and electron (-1), unless we configure otherwise

    bool doConstrainedFit = false;

    Acts::Vector3D b_field = Acts::Vector3D(0, 0, 2_T); // TODO: Get this from MagneticFieldService someday

    /// Vertex constraint covariance matrix
    Acts::ActsSymMatrixD<3> constraintCov = Acts::Vector3D(3_mm, 3_mm, 10_mm).asDiagonal();

    /// Vertex constraint position
    Acts::Vector3D constraintPos = Acts::Vector3D(0, 0, 0);

public:
    ActsPrimaryVertexFitter() : JFactoryT<RecoTrack>(NAME_OF_THIS, "acts_primary_vertex_fitter") {};
    void Init() override;
    void ChangeRun(const std::shared_ptr<const JEvent> &event) override;
    void Process(const std::shared_ptr<const JEvent> &event) override;

};

#endif // _RecoTrackFactory_Acts_h_
