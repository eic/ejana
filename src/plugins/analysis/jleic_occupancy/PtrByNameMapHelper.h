#ifndef EJANA_PTR_BY_NAME_MAP_HELPER_H
#define EJANA_PTR_BY_NAME_MAP_HELPER_H

#include <functional>
#include <map>

template<class PtrType>
class PtrByNameMapHelper {
public:

    /// constructor lambda of function is called to construct an object if
    /// it is not found in map (ptr_by_name) by Get function

    explicit PtrByNameMapHelper(const std::function<PtrType*(const std::string &name)>& obj_ctr_func):
            construct_func(obj_ctr_func)
    {
    }


    /// Gets pointer by name if it is not in map, creates object
    /// by contruction function provided in constructor
    PtrType* Get(const std::string &name)
    {
        if(!ptr_by_name.count(name)) {
            auto object = construct_func(name);
            ptr_by_name[name] = object;
            return object;
        }

        return ptr_by_name[name];
    }

    /// Gets underlying map()
    std::map<std::string, PtrType*>& GetMap()
    {
        return ptr_by_name;
    }

private:
    std::function<PtrType*(const std::string &name)> construct_func;
    std::map<std::string, PtrType*> ptr_by_name;
};
#endif // EJANA_PTR_BY_NAME_MAP_HELPER_H
