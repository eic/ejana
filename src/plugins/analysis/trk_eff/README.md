# Simple tracking efficiency analysis

### CLion arguments

```
-Pplugins=g4e_reader,trk_eff
-Ptrk_eff:verbose_=0
-Ptrk_eff:e_beam_energy=5
-Ptrk_eff:ion_beam_energy=50
-Pjana:DEBUG_PLUGIN_LOADING=1
-Pnevents=100000
-Pnthreads=1
-CPROOT:StartGui=1
/home/romanov/eic/g4e-dev/test/beagle/g4e_output_evt.root
```

Working directory:
```
$PROJECT_DIR$/work
```

Environment variables:
```
JANA_PLUGIN_PATH=$PROJECT_DIR$/cmake-build-debug
```



