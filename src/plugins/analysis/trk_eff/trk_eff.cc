
#include "TrackingEfficiencyProcessor.h"


// Routine used to create our JEventProcessor
#include <JANA/JApplication.h>



extern "C"{
    void InitPlugin(JApplication *app)
    {
        InitJANAPlugin(app);

        app->ProvideService(std::make_shared<JGlobalRootLock>());
        // TODO: Make consistent with the rest of eJANA

        app->Add(new TrackingEfficiencyProcessor(app));
    }
}