#ifndef OPEN_CHARM_PROCESSOR_HEADER
#define OPEN_CHARM_PROCESSOR_HEADER

#include <thread>
#include <mutex>

#include <JANA/JEventProcessor.h>
#include <JANA/JObject.h>
#include <ejana/EServicePool.h>

#include "Math/Point3D.h"
#include "Math/Vector4D.h"
#include "DisPlotsRootOutput.h"
#include <TLorentzVector.h>


class JApplication;

class OpenCharmInputParticle:public JObject {
public:
    uint64_t id;  // unique particle id in the event
    ROOT::Math::PxPyPzMVector p;
//    ROOT::Math::XYZPoint vertex;
   // TVector3 p;
    TVector3 vertex;
    int pdg;
    double charge;
    int mother_id;
    int mother_pdg;
    int grand_mother_id;
    int grand_mother_pdg;
};


class DisPlotsProcessor : public JEventProcessor
{
  public:

    // Constructor just applies
    explicit DisPlotsProcessor(JApplication *app=nullptr):
                JEventProcessor(app),
                services(app)
                {};

    // This is called once before the first call to the Process method
    // below. You may, for example, want to open an output file here.
    // Only one thread will call this.
    void Init() override;

    //----------------------------
    // Process
    //
    // This is called for every event. Multiple threads may call this
    // simultaneously. If you write something to an output file here
    // then make sure to protect it with a mutex or similar mechanism.
    // Minimize what is done while locked since that directly affects
    // the multi-threaded performance.
    void Process(const std::shared_ptr<const JEvent>& event) override;


    //----------------------------
    // Finish
    //
    // This is called once after all events have been processed. You may,
    // for example, want to close an output file here.
    // Only one thread will call this.
    void Finish() override;

private:
    DisPlotsRootOutput root_out;
    ej::EServicePool services;
    int verbose;                // verbose output level
    int smearing;                // smearing level
    double e_beam_energy = 10;
    double ion_beam_energy = 100;
    int ncdis; // NCDIS=1, CCDIS=0; 
};

#endif   // OPEN_CHARM_PROCESSOR_HEADER
