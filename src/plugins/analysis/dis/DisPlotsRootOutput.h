#ifndef OPEN_CHARM_ROOT_OUTPUT_HEADER
#define OPEN_CHARM_ROOT_OUTPUT_HEADER

#include <TFile.h>
#include <TDirectory.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TTree.h>
#include <TApplication.h>

class DisPlotsRootOutput
{
public:
    void init(TFile *file)
    {
        // 'locker' locks mutex so other threads can't interfere with TFile doing its job
        std::lock_guard<std::recursive_mutex> locker(lock);        

        // create a subdirectory "hist_dir" in this file
        file->cd();         // Just in case some other root file is the main TDirectory now
        plugin_root_dir = file->mkdir("dis_plots");
        plugin_root_dir->cd();

        // TTree with recoiled electron
        tree_rec_e = new TTree("rec_e", "a Tree with vect");
        tree_rec_e->SetDirectory(plugin_root_dir);

        //------------------------------------ High-x  -----------------------------------------------------
        const int XBINS_h=19, Q2BINS_h=16;
        double xEdges_h[XBINS_h + 1] =
                {-3.8, -3.6, -3.4, -3.2, -3.,
                 -2.8, -2.6, -2.4, -2.2, -2.,
                 -1.8, -1.6, -1.4, -1.2, -1.,
                 -0.8, -0.6, -0.4, -0.2, 0.};


        Double_t Q2Edges_h[Q2BINS_h + 1] =
                {0.,   0.25,  0.5,  0.75, 1.,
                 1.25, 1.5,   1.75, 2.,   2.25,
                 2.5,  2.75,  3.,   3.25, 3.5,
                 3.75, 4.};

	    //--------- true DIS values---------
        h2_XQ2_true_log = new TH2I("XQ2_true_log","True X,Q2 ", XBINS_h,xEdges_h, Q2BINS_h, Q2Edges_h);
        h2_XQ2_true = new TH2I("XQ2_true","True X,Q2 ", 100,0.,1., 100, 1.,10.);
        h2_XQ2_true->GetXaxis()->SetTitle("x_{true}");
        h2_XQ2_true->GetYaxis()->SetTitle("Q^{2}_{true}");

        h1_Q2_true = new TH1D("Q2_true", "True Q2 ", 100, 0, 10);
        h1_Q2_true->GetXaxis()->SetTitle("Q2_{true}");

        h1_x_true = new TH1D("x_true", "True X ", 100, 0., 1.);
        h1_x_true->GetXaxis()->SetTitle("x_{true}");

        h1_y_true = new TH1D("y_true", "True Y ", 100, 0., 1.);
        h1_y_true->GetXaxis()->SetTitle("y_{true}");

        //--------- 4v method ---------
        h2_XQ2_4v = new TH2I("XQ2_4v","4v  X,Q2 ", 100,0.,1., 100, 1.,10.);
        h2_XQ2_4v->GetXaxis()->SetTitle("x_{4v}");
        h2_XQ2_4v->GetYaxis()->SetTitle("Q^{2}_{4v}");

        h2_XQ2_4v_log = new TH2I("XQ2_4v_log","4v X,Q2 log ", XBINS_h,xEdges_h, Q2BINS_h, Q2Edges_h);
        h2_XQ2_4v_log->GetXaxis()->SetTitle("log10(x_{4v})");
        h2_XQ2_4v_log->GetYaxis()->SetTitle("log10(Q^{2}_{4v})");
   
        h2_XQ2_4v_cuts = new TH2I("XQ2_4v_cuts ","4v X,Q2 cuts ", XBINS_h,xEdges_h, Q2BINS_h, Q2Edges_h);
        h2_XQ2_4v_cuts->GetXaxis()->SetTitle("log(x_{4v})");
        h2_XQ2_4v_cuts->GetYaxis()->SetTitle("log(Q^{2}_{4v})");

        h1_Q2_4v = new TH1D("Q2_4v", "4v Q2 ", 100, 0, 5000);
        h1_Q2_4v->GetXaxis()->SetTitle("Q2_{4v}");

        h1_X_4v = new TH1D("X_4v", "4v X ", 100, 0., 1.);
        h1_X_4v->GetXaxis()->SetTitle("x_{4v}");

        h1_Y_4v = new TH1D("Y_4v", "4v Y ", 100, 0., 1.);
        h1_Y_4v->GetXaxis()->SetTitle("y_{4v}");

        //----- electron Method --------------------
        h2_XQ2_em = new TH2I("XQ2_em"," EM X,Q2 ", 100,0.,1., 100, 1.,10.);
        h2_XQ2_em->GetXaxis()->SetTitle("x_{em}");
        h2_XQ2_em->GetYaxis()->SetTitle("Q^{2}_{em}");
  
        h2_XQ2_em_log = new TH2I("XQ2_em_log","EM X,Q2 log ", XBINS_h,xEdges_h, Q2BINS_h, Q2Edges_h);
        h2_XQ2_em_log->GetXaxis()->SetTitle("log10{x_{em})");
        h2_XQ2_em_log->GetYaxis()->SetTitle("log10(Q^{2}_{em})");
  
        h2_XQ2_em_cuts = new TH2I("XQ2_em_cuts ","EM X,Q2 cuts ", XBINS_h,xEdges_h, Q2BINS_h, Q2Edges_h);
        h2_XQ2_em_cuts->GetXaxis()->SetTitle("log(x_{em})");
        h2_XQ2_em_cuts->GetYaxis()->SetTitle("log(Q^{2}_{em})");

        h1_Q2_em = new TH1D("q2_em", "Em Q2 ", 100, 0, 5000);
        h1_Q2_em->GetXaxis()->SetTitle("Q2_{em}");

        h1_x_em = new TH1D("x_em", "Em X ", 100, 0., 1.);
        h1_x_em->GetXaxis()->SetTitle("x_{em}");

        h1_y_em = new TH1D("y_em", "Em Y ", 100, 0., 1.);
        h1_y_em->GetXaxis()->SetTitle("y_{em}");

	    //-------------other ---------------
        int diff_bin_em=100;
        h2_MxY_em  = new TH2I("MxY_em","hMxY_em",   diff_bin_em,0. , 100., 100, 0. , 1.);

        h2_Xem_diff  = new TH2F("Xem_diff" ,"(x_{em}-x_{true})/x_{true} vs x_{true}"    , diff_bin_em, -2.8 , 0. ,  diff_bin_em, -1.   ,1.  );
        h2_Xem_diff->GetXaxis()->SetTitle("log(x_{true})"); h2_Xem_diff->GetYaxis()->SetTitle("(x_{em}-x_{true})/x_{true}");


        h2_Yem_diff  = new TH2F("Yem_diff" ,"(y_{em}-y_{true})/y_{true} vs y_{true}"    , diff_bin_em, -2.8 , 0. ,  diff_bin_em, -1.   ,1.  );
        h2_Yem_diff->GetXaxis()->SetTitle("log(y_{true})");
        h2_Yem_diff->GetYaxis()->SetTitle("(y_{em}-y_{true})/y_{true}");

        h2_Q2em_diff  = new TH2F("Q2em_diff" ,"(Q^{2}_{em}-Q^{2}_{true})/Q^{2}_{true} vs Q^{2}_{true}"    ,  diff_bin_em, -2. , 4. ,  diff_bin_em, -1.   , 1.  );
        h2_Q2em_diff->GetXaxis()->SetTitle("log(Q^{2}_{true})");
        h2_Q2em_diff->GetYaxis()->SetTitle("(Q^{2}_{em}-Q^{2}_{true})/Q^{2}_{true}");

        h1_x_diff_em  = new TH1F("x_diff_em" , "x_{em}-x_{true})/x_{true}  "     , diff_bin_em, -2.8 , 0.   );
        h1_x_diff_em->GetXaxis()->SetTitle("log(x_{true})");
        h1_x_diff_em->GetYaxis()->SetTitle("(x_{em}-x_{true})/x_{true}");

        h1_y_diff_em  = new TH1F("y_diff_em" , "y_{em}-y_{true})/y_{true}  "     , diff_bin_em, -2.8 , 0.   );
        h1_y_diff_em->GetXaxis()->SetTitle("log(y_{true})");
        h1_y_diff_em->GetYaxis()->SetTitle("(y_{em}-y_{true})/y_{true}");

        h1_q2_diff_em  = new TH1F("q2_diff_em" , " Q^{2}_{em}-Q^{2}_{true})/Q^{2}_{true}  "     , diff_bin_em, -2. , 4.   );
        h1_q2_diff_em->GetXaxis()->SetTitle("log(Q^{2}_{true})");h1_q2_diff_em->GetYaxis()->SetTitle("(Q^{2}_{em}-Q^{2}_{true})/Q^{2}_{true}");

        //----------------------------------------
        h1_empz=  new TH1F("empz" , " E- Pz electron " , 100, 0., 50.   );

        h1_event_num = new TH1D("num_evt", "Event N", 100, 0., 1000.);
        h1_event_num->GetXaxis()->SetTitle("Event Number");

        h1_particle_num = new TH1D("num_part", "Number of particles in an event", 101, 0., 100.);
        h1_particle_num->GetXaxis()->SetTitle("Number of particles");
    }

    // ---- kinematic variables---

    TH2I *h2_XQ2_true, *h2_XQ2_em,  *h2_XQ2_4v;
    TH2I *h2_XQ2_true_log, *h2_XQ2_em_log,  *h2_XQ2_4v_log;
    TH2I *hXQ2_em_smear;
    TH2I *h2_XQ2_em_cuts, *h2_XQ2_4v_cuts;
    TH1D *h1_x_true, *h1_Q2_true, *h1_y_true;
    TH1D *h1_x_em, *h1_Q2_em, *h1_y_em;
    TH1D *h1_X_4v, *h1_Q2_4v, *h1_Y_4v;


    //--------------- other ----- 
    TH2I *h2_MxY_em;
    TH1F *h1_empz;
    TH2F *h2_Xem_diff,*h2_Yem_diff,*h2_Q2em_diff;
    TH1F *h1_x_diff_em,*h1_y_diff_em,*h1_q2_diff_em;

    TH1D *h1_particle_num;
    
    TH1D *h1_event_num;
    std::recursive_mutex lock;

    TTree * tree_rec_e;     // Tree to store electron related data
    

private:

    TDirectory* plugin_root_dir;   // Main TDirectory for Plugin histograms and data
};

#endif // OPEN_CHARM_ROOT_OUTPUT_HEADER
