/** @warning (!) This file is temporary. It is created to mimic upcoming JANA2 API
 *
 *  Converts existing JApplication, EApplication and ERootApplication to a collection of 'services'
 *  configured at runtime initialization
 */
#ifndef EJANA_ESERVICEPOOL_H
#define EJANA_ESERVICEPOOL_H

#include <JANA/JApplication.h>
#include <TFile.h>
#include <fmt/ostream.h>

#include "EJanaRootApplication.h"

class JParameterManager;

namespace ej {

    template <typename T>
    inline T* get_service(JApplication*){
        return nullptr;
    }

    template <>
    inline JParameterManager* get_service<JParameterManager>(JApplication *app) {
        return app->GetJParameterManager();
    }

    template <>
    inline TFile* get_service<TFile>(JApplication *app) {

        auto root_app = static_cast<EJanaRootApplication*>(app);

        if(root_app) {
            return root_app->get_output_root_file();
        }

        fmt::print(std::cerr, "Some function asked for <TFile> service. "
                              "But Application is not EJanaRootApplication."
                              "Looks something is going to crash soon...");
        return nullptr;
    }



    class EServicePool{
    public:

        explicit EServicePool(JApplication *app)
            :app_(app)
        {
        }

        template<typename T>
        T* Get(){return get_service<T>(app_);}



    private:
        JApplication *app_;
    };
}


#endif //EJANA_ESERVICEPOOL_H
