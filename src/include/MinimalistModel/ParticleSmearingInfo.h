#ifndef EJANA_PARTICLESMEARINGINFO_H
#define EJANA_PARTICLESMEARINGINFO_H

namespace minimodel {

    struct ParticleSmearingInfo {
        bool     has_e;           /// Energy was smeared
        bool     has_p;           /// Momentum was smeared
        bool     has_pid;         /// PID is smeared
        bool     has_vtx;         /// Vertex is smeared
        bool     has_any_eppid;   /// Has smeared e, p, or pid
        double   orig_tot_e;      /// original Energy
        double   orig_p;          /// original total momentum
        double   orig_px;         /// original px
        double   orig_py;         /// original py
        double   orig_pz;         /// original pz
        double   orig_vtx_x;      /// original px
        double   orig_vtx_y;      /// original py
        double   orig_vtx_z;      /// original pz
    };
}


#endif //EJANA_PARTICLESMEARINGINFO_H

