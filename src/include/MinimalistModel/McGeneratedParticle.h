#ifndef EJANA_MCGENERATEDPARTICLE_HH
#define EJANA_MCGENERATEDPARTICLE_HH

#include <JANA/JObject.h>

#include <cstdint>

#include "ParticleSmearingInfo.h"

namespace minimodel
{
    class McGeneratedParticle: public JObject
    {
    public:
        uint64_t count;
        uint64_t id;                    /// Unique ID inside event
        int64_t  pdg;                   /// PDG value
        uint64_t trk_id;                /// ID of the related track
        double   charge;                /// no comments
        double   dir_x;                 /// Direction unit vector component at vertex
        double   dir_y;
        double   dir_z;
        double   p;                     /// total momentum
        double   px;                    /// dir_x * p
        double   py;                    /// dir_y * p
        double   pz;                    /// dir_z * p
        double   tot_e;                 /// total energy
        double   m;                     /// m for mass or murder... or mass mu...
        double   time;
        bool     is_beam;               /// Is beam particle
        bool     is_stable;             /// If=true means final state particle
        uint64_t gen_code;              /// Code from generator like kfv
        uint64_t mother_id;             /// mother (first mother) pid
        uint64_t mother_second_id;      /// mother pid

        // polarization info
        double   has_pol_info=false;    /// Has information about the polarization
        double   pol_x;                 /// polarization
        double   pol_y;
        double   pol_z;

        // vertex info
        bool     has_vtx_info=false;    /// Has vertex info
        uint64_t vtx_id;                /// Generated vertex ID
        double   vtx_x;                 /// vertex x
        double   vtx_y;                 /// vertex y
        double   vtx_z;                 /// vertex z
        double   vtx_t;                 /// vertex t

        // smearing info
        bool     has_smear_info=false;  /// Was smearing module enabled or not
        ParticleSmearingInfo smear;     /// Smearing information
    };
}

#endif //EJANA_MCGENERATEDPARTICLE_HH
