// $Id$
//
//    File: VertexTrackerCluster.h
// Created: Thu Jan  5 21:59:51 EST 2017
// Creator: davidl (on Darwin harriet 15.6.0 i386)
//

#ifndef _VertexTrackerCluster_
#define _VertexTrackerCluster_

#include <JANA/JObject.h>

#include "RecoHit.h"

class RecoTrackCand : public JObject{
public:
		
    // Container of hits in the cluster.
    // Technically we could just keep them as associated
    // objects and not define this container. However, this
    // is a little more transparent.
    std::vector<const RecoHit*> hits;

    uint64_t track_id;  // 0 denotes N/A
};

#endif // _VertexTrackerCluster_

