import unittest
from pyjano.jana import Jana
import os

class TestStringMethods(unittest.TestCase):

    def test_vmeson_beagle(self):
        Jana(nevents=1000, output='eic_pythia_lund_reader.root')\
            .plugin('lund_reader') \
            .plugin('vmeson') \
            .plugin('event_writer')\
            .source('./eic-pythia_ep-10x100_10evt_rad-cor-cut.lund')\
            .run(retval_raise=True)


if __name__ == '__main__':
    unittest.main()
