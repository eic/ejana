import unittest
import os


class TestG4eReader(unittest.TestCase):

    def setUp(self) -> None:
        self.file_name = 'g4e_v3_3ev.root'

    def test_g4e_reader(self):
        self.assertEqual('foo'.upper(), 'FOO')
        from pyjano.jana import Jana
        Jana(nevents=10000, output='beagle.root')\
            .plugin('g4e_reader') \
            .plugin('g4e_reader_test') \
            .source('./g4e_v3_3ev.root')\
            .run(retval_raise=True)

if __name__ == '__main__':
    unittest.main()
